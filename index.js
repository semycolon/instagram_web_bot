const {IG} = require('./libs/ig_lib');
const {log} = require('./libs/log_lib');
const {DB} = require('./libs/db_lib');

async function main() {
    await DB.connect();
    let client = await IG.get_logged_in_client();
    // await add_targets_by_username_followers(60);
    let targets = await DB.get_targets(1);
    let all = await IG.get_all_photo_by_username(client, 'instagram',56);
    let result = await DB.insert_photos_to_targets_photos(all);
    result;
}

async function follow_scenario() {
    let targets = await DB.get_targets(20);
    await follow_targets(client, targets);
}

async function fetch_targets_by_followers(count) {
    let client = await IG.get_logged_in_client();
    let target_profile = await IG.get_user_by_username(client, "semycolon_");
    let followers = await IG.get_all_followers(client, target_profile.id, count);
    //todo add targets ...
}

async function follow_targets(client, targets) {
    log.debug("follow_targets called()", targets.length);
    console.log(typeof targets);
    let userId_list = targets.map(person => person.id);
    userId_list;
    on_follow_success = function (userId) {
        DB.remove_target(userId, true);
    };
    await IG.follow_bulk(client, userId_list, on_follow_success)
}

main();

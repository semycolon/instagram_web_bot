const ObjectId = require("mongoose").Types.ObjectId;
module.exports.generateRandomString = function (length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
};

module.exports.generateRandomNumber = function (length) {
	var text = "";
	var possible = "0123456789";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
};

module.exports.isObjectId = function(arg){
	try{
		return String(new ObjectId(arg)) === String(arg)
	}catch (e) {
		return false;
	}
};


async function asyncForEach (array, callback) {
	if (array && array instanceof Array) {
		for (let index = 0; index < array.length; index++) {
			await callback(array[index], index);
		}
	} else {
		console.log('Error array must be a non-null array');
	}
}
module.exports.asyncForEach = asyncForEach;

let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/";
const {log} = require('./log_lib');

let dbo;

const collections = {
    targets : "targets",
    targets_photos : "targets_photos",
    shots: "shots",
    shots_photos: "shots_photos",
};

async function connect() {
    try {
        dbo = await _getDatabase();
        log.info("Database Connection initialized :)")
    }
    catch (err) {
        log.error(err);
        throw new Error("Database Connection Failed.");
    }
}

function _getDatabase() {
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                reject(err);
            }
            let dbo = db.db("ig_bot");
            resolve(dbo);
        });
    })
};

async function get_targets(count) {
    _check_init();
    return await dbo.collection(collections.targets).find({}).limit(count).toArray()
}
async function get_targets_photos(count) {
    log.debug("get_targets_photos called()", count);
    _check_init();
    return await dbo.collection(collections.targets_photos).find({}).limit(count).toArray()
}

function _check_init() {
    if (!dbo) {
        throw new Error("Database has not been initialized , you must call connect() before.")
    }
}

async function add_targets(followers) {
    _check_init();
    try {
        let res = await dbo.collection(collections.targets).insertMany(followers, {ordered: false});
        log.info(res);
        await dbo.collection(collections.targets).createIndex({'username': 1}, {unique: true});
    } catch (err) {
        if (err && err.errmsg && err.errmsg.toString().startsWith("E11000 duplicate key error")) {
            //ignored ...
        }else{
            log.error(err);
            log.error("add_targets Had Some Errors")
        }
    }
}

async function insert_photos_to_targets_photos(photos) {
    log.debug("insert_photos_to_targets_photos called()",photos.length );
    _check_init();
    try {
        let res = await dbo.collection(collections.targets_photos).insertMany(photos, {ordered: false});
        log.info(res);
        let result = await dbo.collection(collections.targets_photos).createIndex({'username': 1}, {unique: true});
        return result.status;
    } catch (err) {
        log.error(err);
        log.error("insert_photos_to_targets_photos Had Some Errors")
    }

}

async function remove_target(target_id, addToShotList = false) {
    _check_init();
    try {
        if (addToShotList) {
            let follower = await get_target_by_id(target_id);
            if (follower) {
                try {
                    try {
                        let res = await dbo.collection(collections.shots).insertOne(follower, {ordered: false});
                        if (res && res.result) log.info("INsert Shot" ,res.result);
                        await dbo.collection(collections.shots).createIndex({'username': 1}, {unique: true});
                    } catch (err) {
                        if (err && err.errmsg && err.errmsg.toString().startsWith("E11000 duplicate key error")) {
                            //ignored ...
                        }else{
                            log.error(err)
                        }
                    }
                } catch (err) {
                    log.error(err);
                }
            }
        }
        let res = await dbo.collection(collections.targets).deleteOne({id: target_id});
        if (res && res.result) log.info("Delete Target " ,res.result);
    } catch (err) {
        log.error(err);
    }

}

async function remove_target_photo(id, addToShotList = false) {
    log.debug("remove_target_photo called()",id,addToShotList);
    _check_init();
    try {
        if (addToShotList) {
            let photo = await get_target_photo_by_id(id);
            if (photo) {
                try {
                    try {
                        let res = await dbo.collection(collections.shots_photos).insertOne(photo, {ordered: false});
                        if (res && res.result) log.info("INsert Shot" ,res.result);
                        await dbo.collection(collections.shots_photos).createIndex({'username': 1}, {unique: true});
                    } catch (err) {
                        if (err && err.errmsg && err.errmsg.toString().startsWith("E11000 duplicate key error")) {
                            //ignored ...
                        }else{
                            log.error(err)
                        }
                    }
                } catch (err) {
                    log.error(err);
                }
            }
        }
        let res = await dbo.collection(collections.targets_photos).deleteOne({id: id});
        if (res && res.result) log.info("Delete Target " ,res.result);
    } catch (err) {
        log.error(err);
    }

}

async function get_target_by_id(id) {
    return await dbo.collection(collections.targets).findOne({id})
}
async function get_target_photo_by_id(id) {
    return await dbo.collection(collections.targets_photos).findOne({id})
}

let DB = {connect, get_targets, add_targets, remove_target , insert_photos_to_targets_photos,remove_target_photo , get_targets_photos};
module.exports = {DB};

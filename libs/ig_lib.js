const utils = require('./utils_lib');
require('dotenv').config();
const Instagram = require('instagram-web-api');
//colorful terminal output
const {log} = require('./log_lib');
var shell = require('shelljs');
const path = require('path');
async function get_logged_in_client() {
    log.debug("get_logged_in_client() called ");
    const {username, password} = process.env;
    const FileCookieStore = require('tough-cookie-filestore2');
    //ensure cookie path exists
    let dir_path = path.resolve("./cookies/" + username);
    shell.mkdir('-p', dir_path);
    const cookieStore = new FileCookieStore(dir_path+'/cookies.json');
    const client = new Instagram({username, password, cookieStore});
    log.info("Logging in with ", username, password);
    const {authenticated, user} = await client.login();

    /*
      todo issue this
    if(!user){
          throw new Error("Username is not valid");
      }*/
    if (!authenticated) {
        throw new Error("password is not valid");
    }
    log.info("Logged In Successfully :)");
    return client;
}

async function get_profile(client) {
    log.debug("Get Profile () Called ");
    const profile = await client.getProfile();
    return profile;
}

async function get_user_by_username(client, username) {
    log.debug("get_user_by_username() called", new Date());
    const result = await client.getUserByUsername({username});
    return result;
}

async function logout(client) {
    await client.logout()
}

var GET_FOLLOWER_RATE_LIMIT_ECEEDED = false;

async function get_followers(client, userId, after = null,should_sleep = false) {
    log.debug("get_followers called()" , log._convert_arguments_to_string(arguments));
    try {
        const followers = await client.getFollowers({userId, after, first: 50});
        if(should_sleep){
            await sleep_random(30, 60);
        }
        return followers;
    } catch (err) {
        //check for rate limit
        if (err.statusCode && err.statusCode === 429) {
            log.error("RateLimit Error", err.error.message);
            GET_FOLLOWER_RATE_LIMIT_ECEEDED = true;
        } else if (err.statusCode) {
            log.error("StatusCode is", err.statusCode, "Message:", err.error.message)
        } else {
            log.error(err);
            console.log(err);
        }
        log.info("Sleeping for 15 Minutes ...");
        await _sleep(60 * 15);
        return get_followers(client, userId, after,true)
    }
}
async function get_photos_by_username(client, username , after = null,should_sleep = false) {
    log.debug("get_photos_by_username called()" , username, after );
    try {
        const photos = await client.getPhotosByUsername({username , after, first: 25});
        photos
        if(should_sleep){
            await sleep_random(10, 30);
        }
        if (photos && photos.user && photos.user.edge_owner_to_timeline_media &&  photos.user.edge_owner_to_timeline_media.edges) {
            return {list : photos.user.edge_owner_to_timeline_media.edges ,
                page_info : photos.user.edge_owner_to_timeline_media.page_info };
        }else{
            throw new Error("Check this uout");
        }
    } catch (err) {
        //check for rate limit
        if (err.statusCode && err.statusCode === 429) {
            log.error("RateLimit Error", err.error.message);
        } else if (err.statusCode) {
            log.error("StatusCode is", err.statusCode, "Message:", err.error.message)
        } else {
            log.error(err);
            console.log(err);
        }
        log.info("Sleeping for 15 Minutes ...");
        await _sleep(60 * 15);
        return get_photos_by_username(client, username, after,true)
    }
}

async function get_followings(client, userId, after = null,should_sleep = false) {
    log.debug("get_followers called()");
    try {
        const followers = await client.getFollowings({userId, after, first: 50});
        await sleep_random(30, 60);
        return followers;
    } catch (err) {
        //check for rate limit
        if (err.statusCode && err.statusCode === 429) {
            log.error("RateLimit Error", err.error.message);
            GET_FOLLOWER_RATE_LIMIT_ECEEDED = true;
        } else if (err.statusCode) {
            log.error("StatusCode is", err.statusCode, "Message:", err.error.message)
        } else {
            log.error(err);
            console.log(err);
        }
        if(should_sleep){
            log.info("Sleeping for 15 Minutes ...");
            await _sleep(60 * 15);
        }
        return get_followings(client, userId, after)
    }
}

async function get_all_photo_by_username(client, username , count) {
    let list = [];
    //for first time load
    let has_next_page = true;
    let next_page = undefined;
    let should_wait = false;
    while ( has_next_page && list.length <= count) {
        let photos = await get_photos_by_username(client, username, next_page , should_wait);
        photos
        photos.list.forEach(it => {
            list.push(it.node);
        });
        next_page = photos.page_info.end_cursor;
        has_next_page = photos.page_info.has_next_page;
        has_next_page
        should_wait = true;
        log.debug("Fetched ", list.length, "Photos Sofar");
    }
    return list;
}


async function get_all_followers(client, userId, count) {
    let list = [];
    //for first time load
    let has_next_page = true;
    let next_page = undefined;
    let should_wait = false;
    while ( has_next_page && list.length <= count) {
        let followers = await get_followers(client, userId, next_page , should_wait);
        followers.data.forEach(person => {
            list.push(person);
        });
        next_page = followers.page_info.end_cursor;
        has_next_page = followers.page_info.has_next_page;
        should_wait = true;
        log.debug("Fetched ", list.length, "People Sofar");
    }
    return list;
}

async function get_all_followings(client, userId, count) {
    let list = [];
    //for first time load
    let has_next_page = true;
    let next_page = undefined;
    while ( has_next_page && list.length <= count) {
        followers = await get_followings(client, userId, next_page);
        followers.data.forEach(person => {
            list.push(person);
        });
        next_page = followers.page_info.end_cursor;
        has_next_page = followers.page_info.has_next_page;
        log.debug("Fetched ", list.length, "People Sofar");
    }
    return list;
}

const _sleep = (seconds) => {
    log.info("Sleeping for ", seconds, " Seconds ... ");
    return new Promise(resolve => setTimeout(resolve, seconds * 1000))
};

function _getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

async function sleep_random(min, max) {
    await _sleep(_getRandomInt(min, max));
}

async function get_activity(client) {
    const activity = await client.getActivity();
    return activity;
}

async function main() {
    try {
        let client = await get_logged_in_client();
        let SemyColon_ = await get_user_by_username(client, "semycolon_");
        // let followers = await get_all_followers(client, SemyColon_.id, 500);
        //log.debug(followers);
        //log.debug(JSON.stringify(followers));
        log.info(activies);
    } catch (err) {
        log.error(err);
        console.log(err);
    }
}

async function follow_bulk(client , userId_list,on_success_callback = () => {}){
    log.debug("follow_bulk called() with ", userId_list.length , " ids.");
    let requested = 0;
    let followed = 0;
    let error = 0;
    await utils.asyncForEach(userId_list, async (userId, index) => {
        try {
            let res = await follow_person(client, userId, true);
            if (res.status === "requested") {
                requested++;
            } else if (res.status === "following") {
                followed++;
            }
            on_success_callback(userId);
        } catch (err) {
            log.error(err);
            error++;
        }
    });
    return {requested,followed, error};
}

async function follow_person(client ,userId, should_sleep = false) {
    log.debug("follow person called() with userID",userId );
    try {
        let res = await client.follow({userId});
        if (should_sleep){
            await sleep_random(5, 15);
        }
        log.debug("Follow Result ",res);
        if (res.status && res.status === "ok") {
            return res;
        }else{
            log.debug("ERROR", "Follow response is not OK ...");
        }
        return res;
    } catch (err) {
        log.error(err);
        log.info("Sleeping for 5 minutes ...");
        await _sleep(60 * 5);
        return follow_person(client,userId,should_sleep);
    }

}

async function like_media_bulk(client , mediaId_list){
    log.debug("like_bulk called() with ", meidaId_list.length , " ids.");
    await utils.asyncForEach(meidaId_list, async (userId, index) => {
        let res = await like_media(client, userId, true);
    })
}

async function like_media(client ,userId, should_sleep = false) {
    log.debug("like person called() with userID",userId );
    try {
        let res = await client.like({userId});
        if (should_sleep){
            await sleep_random(5, 15);
        }
        log.debug("Follow Result ",res);
        return res;
    } catch (err) {
        log.error(err);
        log.info("Sleeping for 5 minutes ...");
        await _sleep(60 * 5);
        return like_person(client,userId,should_sleep);
    }

}

const IG = {
    get_logged_in_client,
    get_activity,
    get_followings,
    get_followers,
    get_all_followers,
    get_user_by_username,
    get_all_followings,
    logout,
    get_profile,
    follow_person,
    follow_bulk,
    get_photos_by_username,
    get_all_photo_by_username
};

module.exports = { IG };


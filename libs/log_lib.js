
let colors = require('colors');

function info(){
    let str = _convert_arguments_to_string(arguments);
    console.log((_date()+" INFO ").bgGreen , str.green);
}
function debug(){
    let str = _convert_arguments_to_string(arguments);
    console.log((_date()+" DEBUG ").bgBlue , str.blue);
}

function _convert_arguments_to_string(args) {
    let str = "";
    for (let i = 0; i < args.length; i++) {
        try {
            str += JSON.stringify(args[i]) + " ";
        } catch (e) {
        }
    }
    return str;
}

const dateformat = require("dateformat");
function _date() {
    return dateformat(new Date(),"h:MM:ss")
}

function error(){
    if (arguments[0] instanceof  Error){
        console.error(arguments[0]);
    }
    let str = _convert_arguments_to_string(arguments);
    console.log((_date()+" ERROR ").bgRed , str.red);
}

let log = { info ,  debug, error , _convert_arguments_to_string};
module.exports = {log};
